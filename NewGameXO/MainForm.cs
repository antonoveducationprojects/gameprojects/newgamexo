﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NewGameXO
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		Pen myPen = new Pen(Color.Black);
		SolidBrush bgBrush=new SolidBrush(Color.Green);
		SolidBrush pixelBrush=new SolidBrush(Color.Aqua);
		Int32 rectWidth=200;
		protected Graphics gDev;
		protected GameController game;
		public MainForm()
		{
			InitializeComponent();
			gDev=Graphics.FromHwnd(paintArea.Handle);
			game=new GameController(gDev);
		}
		void MainFormMouseClick(object sender, MouseEventArgs e)
		{
	
		}
		void MainFormPaint(object sender, PaintEventArgs e)
		{

		}
		void PaintAreaPaint(object sender, PaintEventArgs e)
		{
			Graphics myDev=e.Graphics;
			game.DisplayMap(myDev);
		}
	}
}
