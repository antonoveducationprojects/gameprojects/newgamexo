﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace NewGameXO
{
	/// <summary>
	/// Description of FigureO.
	/// </summary> 
	
	public class GameFild
	{
		Pen currentPen;
		public GameFild()
		{
			currentPen= new Pen(Color.Black,2);
		}
		public void Display(Graphics gDev)
		{
			for(int j=1;j<=2;++j)
		    {
				gDev.DrawLine(currentPen,1,j*200,600,j*200);
				gDev.DrawLine(currentPen,j*200,0,j*200,600);
				//gDev.DrawLine(currentPen,0,0,600,0);
			}
		}
	}	
	
	public abstract class AbstractGameFigure
	{
		public AbstractGameFigure()
		{

		}
		public abstract void Display(Graphics gDev, int x, int y);
	}
	
	
	public class SimpleXFigure : AbstractGameFigure
	{
		protected Pen currentPen;
		public SimpleXFigure():base()
		{
			currentPen= new Pen(Color.Black,8);
		}
		public override void Display(Graphics gDev, int x, int y)
		{
			gDev.DrawLine(currentPen,x+10,y+10,x+190,y+190);
			gDev.DrawLine(currentPen,x+10,y+190,x+190,y+10);
		}
		
	}
}
