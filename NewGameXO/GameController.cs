﻿/*
 * Created by SharpDevelop.
 * User: guyver
 * Date: 27.11.2018
 * Time: 8:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace NewGameXO
{
	/// <summary>
	/// Description of ......
	/// </summary>
	public class GameController
	{
		byte[,] GameMap= new byte[3,3];
		protected Graphics gDev;
		protected GameFild gameFild;
		protected AbstractGameFigure[] figures=new AbstractGameFigure[2];
		public GameController(Graphics dev)
		{
			gDev=dev;
			gameFild=new GameFild();
			figures[0]=new SimpleXFigure();
		}
		public void DisplayMap(Graphics gDev)
		{
			gameFild.Display(gDev);
			figures[0].Display(gDev,0,0);
		}
	}
}
